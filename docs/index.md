![](images/Avatar.jpg)

## ABOUT ME

Hola! Je m'appelle Nely, j'ai 23 ans. Je suis une étudiante en Master 2 à la **Faculté d'Architecture de l'Université Libre de Bruxelles**. J'ai choisi cet atelier d'_Architecture et Design_, parce que l'architecture d'intérieur et le design de mobilier m'intéressent beaucoup depuis le début de mes études.

À part mes études, j'aime bien voyager, découvrir des nouvelles cultures et villes. J'aime bien la photographie, le cinéma et la musique.

## MY BACKGROUND

Je suis mexicaine et à 18 ans j'ai décidé de changer du pays afin de découvrir une autre culture, une autre langue et une autre manière de vivre. Je suis très contente d'avoir choisi de faire mes études en Belgique, ça m'a permis de découvrir des autres manières de penser, apprendre le français et développer mon côté créatif.

L'année passée, j'ai fait Erasmus à Milan dans l'université de  **Politecnico di Milano**. J'ai trop aimé cette experience malgré la situation de Covid. Ça m'a permis d'améliorer mon niveau d'italien. Ainsi que de m'améliorer  dans mes compétences, j'ai découvert des autres programmes que en Belgique n'utilisait pas, tels quel **Rhinoceros 3D**, **Illustrator** et un peu d'**ArchiCad**.
​

## MON OBJET

Pour cet exercice j'ai choisi comme objet le support de mon ordinateur. J'ai choisi cet objet parce que actuellement le mien n'est pas très facile à transporter.
Pour cet exercice j'aimerais bien travailler cet objet afin que soit multifonctionnel, c'est-à-dire l'utiliser comme support d'ordinateur mais aussi comme une petite table à dessiner (taille A4). Aussi deux autres caractéristiques que je voudrais bien travailler sont l'inclinaison et la taille de l'objet afin que je puisse le utiliser aussi pour mon Ipad.

![](images/Support.jpg)
