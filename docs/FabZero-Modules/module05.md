# MODULE 5: SHAPER

Dans ce module, on fait une formation très courte par rapport à l'utilisation de la machine **Shaper**, similaire à une **mini CNC portable**. Cet outil permet d'usiner de manière manuelle des éléments de grande échelle. Vous pouvez trouver à la fin de ce module, des informations supplémentaires sur la machine et son fonctionnement.

![](../images/M05_02.jpg)

      - Profondeur de coupe maximale: 43mm
      - Diamètre du collier: 8 mm
      - Format du fichier importé: SVG

## FONCTIONNEMENT

##### MISE EN PLACE:
  1. D'abord, il faut avoir une surface de travail suffisamment **dégagé** et **stable** afin de n'a pas avoir des problèmes pendant que la machine fonctionne.

  2. Ensuite il faut fixer la planche de découpe sur la surface de travail. Pour faire ceci, il suffit juste de mettre du tape double-face sur la planche. Pour que la planche ne bouge pas pendant qu'on utilise la machine.

  3. On va mettre les **Shaper tapes**, normalement elles doivent être positionnées 15cm au-dessus du point de découpe.

  ![](../images/Tape.jpg)
  
  ![](../images/M05_01.jpg)

  4. Une fois les tapes mises en place, il va falloir scanner la zone de travail avec la machine. On fait ceci pour que la machine numérise son environnement et pour voir si on a suffisamment shaper tape sur la surface.

  ![](../images/M05_03.jpg)

  5. Si tout est ok, on peut importer notre fichier en format **SVG!!**

  6. Ensuite il va falloir placer le dessin sur la zone de découpe, et on va vérifier que tout la zone est accessible pour la machine.

##### DECOUPE:

  1. On ajuste la **vitesse de rotation** par rapport au matériau de découpe et aussi la **profondeur de découpe**.

  2. On allume la fraiseuse et le ventilateur!!

  3. On peut commencer la découpe, pour faire ceci il va falloir déplacer la machine nous-mêmes.

  Il faut suivre la direction qui est montrée sur l'écran.

Pour ce module, on n'a pas fait exercice individuel mais sinon un exercice en groupe.

Voici le résultat final :)

![](../images/M05_00.jpg)


## LINKS

- [Shaper](https://www.shapertools.com/fr-fr/)
- [FabLab guide: Shaper](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/camila.marzano.gontijo/-/blob/main/docs/FabZero-Modules/module05.md)
- [Shaper Guide](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)
