# MODULE 2: FUSION 360

Dans ce module, j'ai appris à utiliser le logiciel de Fusion 360. Ce logiciel m'a permis de modéliser les premieres idées de mon objet: _un support d'ordinateur_. Ce premier essai m'a permis de me rendre compte des différentes formes qu'on peut avoir pour cet objet ainsi que la matérialité et les dimensions.  

Avant de commencer à modéliser mon objet, j'ai fait quelques esquisses afin de choisir une forme.
Pour ce module, j'ai décidé de faire un design très simple, en bois et avec le moins de matière possible.

J'aimerais bien dans le prochains modules, developper ce design. Afin que l'inclinaison de mon support ne soit pas fixe mais sinon flexible par rapport à l'usage.

![](../images/Support.jpg)


## PHASE 01 / LE SKETCH

Le premier pas est de dessiner en élévation mon objet. Pour faire ceci, on va dans la partie **Solide** -> **Sketch**, et on choisit le _plan frontal_.

![](../images/13.jpg)

Pour que mon design soit plus stable, c'est-à-dire que mon objet puisse garder sa forme initiale quand on change les dimensions, j'ai décidé d'ajouter des **Restrictions**.

Pour faire ça, on va dans **Sketch** -> **Restrictions**. Pour ce design j'ai ajouté des restrictions pour les lignes **parallèles** , les **perpendiculaires** et les **horizontales**.

Par rapport aux dimensions, pour modifier les mesures de mon design. J'étais dans **Sketch** -> **Créer** -> **Cote d'esquisse**. Cet outil m'a permis de voir les dimensions de chaque ligne et chaque angle. En cliquant sur la dimension, on peut modifier nous-mêmes. Ainsi on peut utiliser des **paramètres**, qui nous permettront de modifier des dimensions de notre objet sans avoir un changement de la forme.

![](../images/00.jpg)

Ensuite pour avoir les coins un peu arrondis, j'étais dans **Modifier** -> **Épissure**, et j'ai sélectionné juste les lignes qui se croisent. Après que mon esquisse est finie je clique dans **Finir esquisse**

![](../images/01.jpg)

## PHASE 02 / EXTRUDER

La deuxième phase est donner une épaisseur à  mon objet. Pour faire ça, je vais dans **Solide** -> **Modifier** -> **Changer Paramètres**.
J'ajoute deux nouvelles paramètres avec les valeurs suivantes:

![](../images/02.jpg)

Ensuite je vais dans **Solide** -> **Extruder** , et pour la dimension je mets le nom du paramètre, **Épaisseur**.

![](../images/04.jpg)

Pour copier l'objet, je vais dans **Solide** -> **Modifier** -> **Déplacer/Copier**. Je sélectionne l'objet, et pour la _dimension X_, je mets la valeur de mon deuxième paramètre, _Largeur_ qui correspond avec les dimensions de mon ordinateur.

![](../images/12.jpg)

![](../images/03.jpg)

## PHASE 03 / LA PLANCHE

Pour complementer mon design, j'ai testé d'ajouter une planche en bois au-dessus de mes supports.

Pour faire ceci, j'étais dans **Solide** -> **Sketch** et ensuite on doit choisir le _Plan_ où on veut travailler. Pour que cette planche soit inclinée, et qu'elle soit posée au-dessus du _support_. J'ai choisi comme _Plan_ , le plan superior de la base mon objet.

Ensuite je vais dans **Créer** -> **Projection** et j'ai choisi la partie superior des deux supports.

![](../images/14.jpg)

![](../images/06.jpg)

La forme de la planche est un _rectangle_ , pour que la forme soit stable si on change la larguer de mon support. J'ai ajouté des **restrictions**, tels quels _coïncident_ et les _dimensions_ sortant du support (30mm). Je clique sur _finaliser esquisse_ -> _extrude_ -> _paramètre: épaisseur table_.

![](../images/07.jpg)

## PHASE 04 / LES ALTERNATIVES

Finalement, j'ai décidé de tester quelques alternatives en changent les deux paramètres déjà mentionnés.

    Epaisseur -> 30mm

![](../images/11.jpg)

    Epaisseur -> 80mm

![](../images/10.jpg)

    Largeur -> 350mm

![](../images/08.jpg)

    Largeur -> 150mm

![](../images/09.jpg)

## LIENS

[Download Laptop Support.stl](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Objet_00.stl)
