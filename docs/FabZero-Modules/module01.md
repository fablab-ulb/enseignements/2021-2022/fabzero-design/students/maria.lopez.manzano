# I. MODULE 1: INSTALLATION DE GIT

Cette semaine, nous avons commencé le premier module. Nous avons appris les bases du site de GitLab. Ainsi que l'importance de la publication de nos processus dans ce site afin de partager d'une manière simple et facile nos connaissances.

## CONFIGURATION DE GIF

Afin de pouvoir utiliser GitLab directement sur mon ordinateur et pas sur le site même, j'ai dû télécharger et configurer Git. Dans ce [lien](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git), le processus est expliqué très clair. Mais, je vais quand même vous expliquer pas à pas toutes les démarchés que j'ai fait pour que soit plus clair.

Le premier pas est de vérifier dans **Terminal** que Gif est installé dans l'ordinateur. Pour faire ça, je tape

    ![](git --version).

Comme réponse de Terminal, j'ai eu:

    ![](xcode-select: note: no developer tools were found at '/Applications/Xcode.app', requesting install. Choose an option in the dialog to download the command line developer tools).

Ce qui veut dire que le programme n'est pas encore installé.

Suite à cette réponse, j'ai téléchargé Git sans problème. Pour être sûr que tout était en ordre, j'ai vérifié avec la même manipulation qu'au début.

![](../images/Git_Installation.jpg)

Pour finir la configuration de Git il faut s'identifier, en ajoutant des informations personnelles dans **Terminal**.
Pour vérifier que tout est ok, j'ai tapé sur **Terminal** le commande

    ![](git config --global --list)

et j'ai eu la réponse suivante:

![](../images/Git_Donnes.jpg)

## KEY SSH

Dans cette partie, je vais vous expliquer comment j'ai fait pour créer une clé SSH. Avoir une clé SSH permet de connecter son ordinateur avec un serveur distant d’une manière sure. Dans ce [lien](https://docs.gitlab.com/ee/ssh/index.html#generating-a-new-ssh-key-pair), on trouve le processus pour pouvoir créer cette clé. Mais je vais quand même tout expliquer ci-dessous.

D'abord, j'ai choisi la clé **ED25519**, une clé SSH mieux adapté pour mon ordinateur et aussi une clé encore plus sure.

Pour générer cette clé je vais au Terminal et je tape

    ![](ssh-keygen -t ed25519 -C "<SSHclé>")

Ensuite je tape **Enter** deux fois afin de créer le code de ma **clé SSH**.
Les démarches sont montrées dans l'image ci-dessus.

![](../images/SSH_Code.jpg)

Pour pouvoir ajouter cette clé SSH dans le site de Gitlab. Je vais sur mon profil -> **"User settings"** -> **"Key SSH"**. Ici on peut coller le code de la clé qui est déterminé par la terminal et aussi on peut ajouter un titre, dans mon cas j'ai nommé ma clé **"Nely Manzano"** et voilà c'est fait, on a notre clé SSH!

![](../images/M01_SSH.png)

## CLONAGE DU REPOSITARY

Pour cloner mon projet de GitLab, il suffit d'aller sur mon site personnel et copier le lien dans la partie **"Clone with SSH"**.

![](../images/GitLab_Clone.jpg)

On va dans Terminal, je mets _git clone_ + mon URL que j'ai copiée avant et voilà notre site personnel est dans notre ordinateur!

![](../images/Cloning.jpg)

## ATOM

Pour pouvoir travailler mon site dans mon ordinateur, j'ai téléchargé le programme d'**Atom** qui est très simple à télécharger.Pour pouvoir ouvrir mon projet sur Atom, je vais sur **File** -> **Add Project** et je sélectionne le dossier qui vient d'être cloné.

Je peux travailler mon site dans le program **Atom** et envoyer directement sur GitLab. Pour avoir un preview de ce que mon site va ressembler quand je vais l'envoyer sur GitLab, je clique **Packages** -> **Markdown Preview** -> **Toggle Preview**

![](../images/M01_Preview.png)

Pour envoyer au site de GitLab, d'abord j'enregistre les changements faits. Ensuite je vais dans la partie inferior -> **1.Git** -> **2.Stage All** -> **3.Ajouter un commentaire** -> **4.Commit to the main** -> **5.Push**.


![](../images/M01_Push1.jpg)

![](../images/M01_Push3.jpg)

## LIENS

- [GIT](https://git-scm.com)
