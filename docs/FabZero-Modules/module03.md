# MODULE 3: IMPRESSION 3D

Dans ce troisième module, j'ai été formée pour pouvoir utiliser les imprimantEs 3D avec le programme de **PrisaSlicer** .

On peut réaliser une impression 3D grâce au fichier **Fusion 360** obtenu dans l'ancien module. Étant donnée qu'il a été enregistré en format _SLT_, on peut l'ouvrir et modifier dans le programme **PrisaSlicer**.

Le FabLab dispose de six imprimantes **Prusa I3 MK3S**. Une machine créée par des imprimantes ce qui est pratique car si l’une de ces pièces est cassée, une autre imprimante peut créer la pièce manquante.

Pour ce module, je vais vous expliquer tout mon processus de l'impression de mon objet.

## PRISA SLICER

D'abord, j'ai téléchargé le programme de **PrisaSlicer** dans ce [site](https://www.prusa3d.com/drivers/). J'ai choisi la version _DRIVERS & APPS 2.2.9.1_ pour MacOS.

![](../images/M03.png)

Ensuite, j'ai ouvert le dossier de mon objet de **Fusion 360**. Mon objet était trop grand donc j'ai réduit sa taille dans la partie _Manipulation de l'objet_.

        Facteur d'échelle: 40%

![](../images/M3_00.jpg)

Afin d'éviter des problèmes au moment de l'impression, j'ai changé la disposition de mon objet de manière horizontale.


## SETTINGS

Pour faire des modifications, c'est important de choisir le _Mode Expert_ ce qui permet d'avoir accès à plus de paramètres qui seront utiles par la suite. Dans cette partie, je vais vous expliquer les réglages que j'ai faits par rapport à mon objet.

**• Couche et périmètre:**

        - Hauteur de couche: 0.2mm

        - Parois verticales: 3

Ceux-ci sont des paramètres à ne pas toucher afin d’avoir une certaine solidité dans l’objet. Nous pouvons modifier le type de remplissage intérieur qui dépend de la résistance de l’objet souhaité. Pour le reste, on ne touche à rien sauf si cela est utile pour l'objet choisi.

![](../images/M3_01.jpg)

**• Remplissage:**

        - Dénsité de remplissage: 10 - 15%

 On peut monter jusqu’à 35% si est vraiment  nécessaire. Les formes de remplissage les plus efficaces sont les formes _gyroïdes_ et en _nid d’abeille_.  

 ![](../images/M3_02.jpg)

**• Jupe et bordure:**

Normalement, il y a déjà une jupe sur l’objet pour que celui-ci soit stable sur le plateau. Par contre, si l'objet est haut, on va créer un contour supplémentaire pour éviter que l'objet tombe.

La _bordure_, si on la rajoute, permet donc d’avoir une plus grande emprise au sol, il s’agit d’une légère couche supplémentaire sur le plateau, autour de notre objet, qui maintient droite la pièce.

![](../images/M3_03.jpg)

**• Supports:**

On peut générer des supports supplémentaires pour maintenir stables des parties de l'objet en _porte à faux_ . Il suffit juste de cocher la case. Dans mon cas je n'ai pas modifié cette partie car le support n'était pas nécessaire étant donné que toute la surface de mon objet était sur la plateforme.

Quelques notes:

- Il vaut mieux laisser par défaut et jouer avec les paramètres si on possède un cas spécial.
- Nettoyer la plateforme avec acétone avant l'usage.
- Il est primordial de surveiller la conception du prototype durant les 3 premières couches afin de s’assurer du bon déroulement de celle-ci.
- Vitesse: 100% et maximum 300%. Plus la machine est lente, plus le design de l’objet sera effectué de manière consciencieuse.

Quand tous les réglages sont en ordre, on va dans **Manipulation de l'objet** -> **Découper maintenant**. Ceci nous permettra de visualiser l'impression de l'objet par couches, ainsi de voir le temps d'impression. (Voir deuxième image)

![](../images/M03_Laminar.jpg)

![](../images/M03_CodeG.jpg)

Si tout est ok, on exporte le **G Code**. On l’importe sur la carte SD qui s’insère dans la machine directement. Pour activer l’impression il suffit de choisir le bon dossier et de le sélectionner. Cette impression était de 30min car c’est un petit objet.

## RESULTAT FINAL

Voici une photo du résultat finale, je suis très contente. L'objet a été très bien imprimé et il ne contient pas des imperfections! :)

![](../images/M03_Foto.jpg)

![](../images/M04_02.jpg)

## LIENS

- [FUSION 360](https://knowledge.autodesk.com/fr/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/FRA/How-to-export-an-STL-file-from-Fusion-360.html)
