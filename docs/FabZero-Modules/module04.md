# MODULE 4: DECOUPE LASER

Ce jeudi, nous avons suivi la formation sur la découpe numérique via un découpage laser. Dans le FabLab nous disposons de deux machines: **Lasersaur** et la **Muse FullSpectrum**.

Dans ce module, j'ai appris à utiliser ces imprimantes ainsi que les réglages à faire avant et pendant l'impression.

## LES IMPRIMANTES

- **Lasersaur:**

Une machine open source qui va être utilisée dans cet exercice. Un des avantages est la qualité de découpe.

Caractéristiques:

      - Surface de découpe: 122cm x 61cm
      - Hauteur maximale: 12 cm
      - Vitesse max: 6000 mm/min (Eviter de monter)
      - Puissance laser: 100 W
      - Type de laser: Tube Co2 (infrarouge)

- **Muse FullSpectrum**

Une machine plus petite qui ne va pas être utilisé dans cet exercice. Elle contient un pointeur rouge qui permet un positionnement  plus précis.  

Caractéristiques:

      - Surface de découpe: 50 cm x 30 cm
      - Hauteur maximale: 6 cm
      - Puissance laser: 40 W
      - Type de laser: Tube Co2 (infrarouge)

## PRECAUTIONS

Afin d'éviter des accidents, il y a une série des démarches qu'il ne faut pas oublier!

     - Connaitre le matériau utilisé (Voir les matériaux interdits).
     - Ouvrir la vanne d’air comprimé
     - Savoir où se trouve le bouton d’urgence.
     - Allumer l’extracteur de fumée.
     - Avoir un extincteur au CO2 à proximité.

## MATERIAUX

 **- Matériaux recommandés:**

      - Bois contreplaqué (3 à 4 mm)
      - Acrylique (Il se découpe très facilement)
      - Papier
      - Carton
      - Textile

**- Matériaux déconseillés:**

      - Le MDF, fumée épaisse et nocive à long terme.
      - ABS et PS
      - Polystyrène (PE, PET, PP)
      - Composites à base de fibres, poussières très nocives
      - Métaux, impossible à découper.

**- Matériaux interdits:**

      - PVC
      - Cuivre (réfléchit au laser)
      - Téflon (PTFE)
      - Vinyle, simili-cuir
      - Résine phénolique, époxy.

Matériaux nocifs pour la santé.

## ESQUISSE

L'énonce de cet exercice c'était imaginer une lampe à partir de notre objet, une lampe influencée par le design de notre objet. Pour faire ceci, d'abord j'ai commencé à analyser mon objet, j'ai remarqué que même si mon objet est assez simple ce qui m'intéressait c'était son inclination. Pour cet exercice je voulais prendre le caractère incliné de mon objet mais d'une manière plus courbée.

En regardant mon objet de manière verticale, je me suis inspiré de la forme de son profil. En pensant que ce profil de manière répétée allait créer une sorte de "rideaux" autour de ma source de lumière.

![](../images/M04_02.jpg)

J'ai commencé à faire des dessins à partir de ces intentions. Finalement, je suis arrivée à l'idée de faire une lampe avec un module identique qui allait se répéter autour de ma source de lumière. L'intention est que ce module aille une forme similaire du profil de l'objet initiale.

![](../images/M04_06.jpg)

## PROTOTYPE

À partir d'une feuille de papier, j'ai commencé à faire des essais en maquette pour voir comment je pouvais créer en 3D mon dessin.

**•Premier test** - (Profil rectiligne)

![](../images/M04_01.jpg)

**•Deuxième test** - (Profil plus ovale)

1. Un Module

![](../images/M04_03.jpg)

2. Une Base

![](../images/M04_04.jpg)

3. Le Prototype

![](../images/M04_00.jpg)

Convaincu de mon deuxième test, j'ai décidé de continuer à travailler sur la forme de son profil et sur la hauteur de la base afin que le design de la lampe ne bascule pas. Ainsi que la manière d'emboiter ces deux parties (les profils et la base).

## IMPRESSION

J'ai dessiné ce modele via _AutoCad_, dans lequel les lignes _bleus_ -> _découpe_ et les lignes _rouges_ -> _pliage_. Pour la découpe laser, normalement on travaille en vectoriel afin que la qualité ne se perds pas si on agrandi le dessin.

![](../images/M04_10.png)

[Download file (SVG)](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Lamp.svg)

**•1ER ETAPE (INKSCAPE):**

      1. Fichier Autocad/Illustrator -> Importé en SVG/DXF.
      2. Quand on insère la clé USB dans l'ordinateur de FabLab, on ouvre le fichier dans Inkscape -> Sélection de l’objet -> clic droit -> Ungroup. Ceci va convertir les pixels en lignes vectorielles.
      3. On vérifie l’échelle du document en **Propriétés du document** -> Je vérifie que la taille du document exporté est correct (Taille: A1).
      Ainsi je vérifie que le document est bien en millimètres et pas en pixeles.  

![](../images/M04_Ink00.png)

      4. J'ai modifié l'épaisseur du trait parce que je n'arrivais pas à voir les traits du document. Pour faire ceci j'étais dans ->** Remplissage et Bordure** -> **Style de trait**

![](../images/M04_Ink.png)

      5. On exporte le fichier en format SVG.


**•2EME ETAPE (DRIVEBOARDAPP):**   

Pour imprimer la lampe, j'ai utilisé la machine **Epilog Laser**, qui fonctionne un peu different de la machine Lasersaur. Je vais vous expliquer ici dessous les démarches que j'ai fait afin d'imprimer.

![](../images/M04_Epilog.jpg)

            1. Installer le matériau dans la machine
            2. Ouvrir le fichier SVG (fichier modifié dans l'étape précédant) dans DriveboardAPP
            3. Remettre le laser à sa position de départ: Home
            4. La source du faisceau laser doit être située au dessus, s'il est en dehors du champ, sélectionnez l'onglet Move. On peut aussi déplacer le projet  dans la partie Offset.
            5. Vérifier que le projet soit situé dans la bonne place.
            6. Régler la distance focale, la hauteur entre lentille et matériau (support suggéré : 15 mm)
            7. Attribuer aux couleurs des lignes une vitesse (F) je laisse à 40% et une puissance (%) de 95% pour une découpe, et pour le pliage je mets (F) - 55% et (%) - 10%.
            8. Mettre les lignes dans l’ordre de découpe:

            1er  - Gravures rouge
            2ème - Découpe à l'intérieur du contour (vert)
            3ème - Découpe de l'objet (bleu)

![](../images/M04_DRIVE.jpg)

            9. Vérifier que le bouton “Status” est en vert. (Sinon clic gauche pour voir d’où vient le problème.)
            10. RUN

NE PAS OUBLIER:

       - Eteindre la machine (boton rouge)
       - Allumer la panne d'air comprimé
       - Allumer l'extracteur de fummée (bouton vert)
       - Allumer le refroidissement à eau (bouton noir)


## RESULTAT FINAL

Après la découpe, le plastic n'a pas été coupe correctement. J'ai dû finir de couper avec un cutter. Je n'ai pas voulu imprimer une deuxième fois afin d'éviter du gaspillage.

Pour créer la lampe, le processus a été le même à celui du papier. Juste pour le pliage, pour le papier était beaucoup plus facile et ici j'ai pris plus de temps à cause de la rigidité du matériau.

Voici une photo de l'objet finale!
Finalement, même si le plastic n'a pas été bien coupé, je suis contente du résultat. :)

![](../images/M04_Finale.jpg)


## LIENS

- [Inkscape](https://inkscape.org/fr/)
- [Guide Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
- [Epilog Guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)
