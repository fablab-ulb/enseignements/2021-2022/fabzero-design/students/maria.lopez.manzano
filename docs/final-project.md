# FINAL PROJECT

Sur cette page, je vais vous expliquer, pas à pas le développement de mon objet final.
Pour cet exercice final, j'ai décidé de changer d'objet finale. Afin de réaliser un objet plus proche à moi et à ma culture.

J'ai choisi un **_"Molcajete"_**, un ustensile de la cuisine mexicaine. Normalement, on l'utilise pour faire marteler des aliments comme tomates, avocates et piments, et créer des sauces.

Depuis que j'étais petite, j'utilisais cet objet pour aider à ma grand-mère et j'avais déjà remarqué certains problématiques. C'est pour cela que j'aimerais bien l'analyser afin de trouver des autres solutions.

![](./images/F_Molcajete.jpg)

    Définition: "Mortier mésoaméricain traditionnel, largement utilisé au Mexique pour broyer ou marteler les aliments, tels que les céréales, les épices et les légumes, destinés à la préparation des sauces et autres plats. Pour moudre les ingrédients, on utilise un cylindre de pierre appelée tejolote, temolote ou communément appelé pierre molcajete."

 ## I. DECONSTRUCTION

`"OBSERVER, ANALYSER ET COMPRENDRE"`

Pour cette première étape, j'ai commencé à analyser les parties et fonctions principales de mon objet.

On trouve principalement 2 parties:

    1. La base: Composée principalement de deux parties, une partie concave où les ingrédients sont mélangés et dans la partie inférieure on trouve trois pattes qui donnent stabilité à l'objet.
    2. Le pylône: "Marteau" pour casser et mêler les aliments.

### MATIERE

Le molcajete traditionnel est réalisé en _pierre volcanique_, ce qui crée que l'objet soit assez lourd et aille une texture assez rugueux.

![](./images/Molcajete.jpg)

### USAGE

Cet ustensile, on l'utilise pour créer des sauces et des guacamoles. À cause de l'irrégularité et la rugosité du matériau,  il est compliqué d'utiliser cet objet pour mêler les épices.  

La particularité de sauces mexicaines est d'avoir une consistance irrégulière, ceci nous ne pouvons pas trouver si on utilise un mixer.
Pour réussir à arriver à un résultat irrégulier la manière d'utiliser le pylône est de manière **circuler** et pas linéaire.

![](./images/F_Finale.jpg)

### EXAMPLES

J'ai regardé un peu sur internet par rapport à des autres manières de créer cet ustensile.
Le même objet existe en autres matières différents:

- **Ciment**
- **Arcille**

![](./images/Ftypes.jpg)


Normalement, la forme reste très similaire mais juste la taille, la matière et la texture varient.

## II. RESOUDRE UN VRAIE PROBLÈME

`"LES CONTRAINTS FONT L'OBJET"`

Après l'analyse et l'usage de cet élément j'ai remarqué certaines problématiques que j'aimerais bien travailler.

#### USAGE:
  -  Texture du matériau:
        - Des morceaux de la nourriture restent dans les petits trous de l'objet, ce qui rend difficile le lavage après l'usage.
        - On ne peut pas l'utiliser pour mêler des épices (ex; poivres)
  - Très lourd, Il est très difficile de le transporter.
  - À cause de la manière d'utiliser l'objet et de la matérialité, l'usage de l'objet est **très fatigant**.

#### FORME:
  - Les trois éléments qui fonctionne comme support, est-ce que sont vraiment nécessaires?
  - Est-ce que la forme de la base peut être plus allongé afin que la main puisse se déplacer d'une manière plus libre ?
  - Est-ce qu'il y a moyen de faire un couvercle?
  - Est-ce qu'il n'y a pas des autres formes du pylône plus efficace?

## III. DIMENSIONS

- Dimensions de base:
       Largueur: 20cm
       Hauteur: 14cm
       Pylône: 5cm x 10cm

![](./images/F_DimMolcajet.jpg)

Afin que l'objet soit plus basé sur moi, j'ai pris aussi les dimensions de ma main. Car l'objet originel est un peu grand pour moi.

## IV. ESQUISSE

#### 1ER TEST: MOLCAJETE SANS SUPPORT

![](./images/F_Supports.jpg)

Pour ce premier test, je voudrais bien tester la forme de mon objet.
J'aimerais bien tester le Molcajete sans les supports et aussi une forme plus allongée à fin que la main puisse se déplacer en cercle d'une manière plus libre.

Aussi j'aimerais bien tester une petite ouverture dans la base pour mettre le pylône.

Pour faire mon premier test, j'y vais dans **Fusion 360** pour tester mon objet en 3D.

1. Nouveau esquisse -> Sélectionne du profil dans lequel on veut travailler -> On dessine le profil de notre objet. Quand on est satisfait de notre dessin, on clique finir esquisse

2. On va dans la commande **révolution** et on sélectionne notre objet.

Même démarche pour la création du pylône.

3. Pour couper la base avec la forme de mon pylône, je vais dans **Modifier** -> **Diviser corps**. Ceci me permets d'avoir l'intersection entre les deux objets, ensuite je sélectionne l'intersection et je **cache** l'objet.

Voici le résultat finale: :)

![](./images/F_Test01_Fusion.jpg)

![](./images/F_Test01_Fusion2.jpg)

Pour l'impression 3D, j'ouvre mon dossier STL en **PrusaSlicer**.

Voici les paramètres que j'ai changé, le reste ne bouge pas:

- Couches et perimètre:
![](./images/F_Test01_01.png)  

- Remplissage:
![](./images/F_Test01_02.png)  

- Jupe et bordure:
![](./images/F_Test01_03.png)  

- Supports:
![](./images/F_Test01_04.png)  

#### RESULTATS

Pour la base, j'ai essayé trois fois l'impression mais malheureusement la machine n'a pas marché correctement.

Pour le pylône, j'ai remarqué que la taille et hauteur me convient. Mais la partie inférieure est très petite ce qui rend plus difficile de mêler les aliments.

![](./images/F_1stTest.jpg)

#### 2EME TEST: FORME DU PYLÔNE

À cause des résultats de la semaine passée, j'ai décidé de continuer à chercher une forme du pylône qui rend l'usage plus efficace, facile et moins fatigant.

Dans les prototypes, j'ai voulu tester deux idées, la première était de prolonger la base du pylône afin que l'activité soit plus facile et mêler les aliments prend moins des temps.

La deuxième idée c'était de changer l'orientation d'usage du pylône. C'est-à-dire au lieu de mêler les aliments de manière circulaire, tester de manière horizontale.

![](./images/F_Schema.jpg)

Le but de ces tests est de voir de quelle manière la main s'est fatigue plus facilement. Ainsi que de voir avec quelle méthode les tomates se mêlent le plus vite.

  **- PROTOTYPES:**

  1. Partie inférieure plus allongée

  - ![](./images/F_Pylone1.jpg)

  - ![](./images/F_Pylone2.jpg)   

  2. Changement du mouvement de la main

  - ![](./images/F_Pylone3.jpg)

  J'ai fait ce prototype circulaire avec quelques "dents" dans la partie inférieur afin que le broyage des aliments soit plus facile.      

  3. Plateau

  - ![](./images/F_Plato.jpg)

Pour le plateau, j'ai fait 4.5 cm de hauteur afin que la main puisse bouger d'une plus libre et qu'elle ne soit pas dérangée par l'hauteur  du plateau.

**- RESULTATS:**

**1. Pylône**

![](./images/F_Prototypes.jpg)

Pour le premier modèle, l'hauteur, forme et épaisseur me convient mais la partie inférieur est encore trop petite pour l'usage.

Pour le deuxième modèle, l'impression a bien marché, la taille me convient mais peut-être il pourrait être un petit plus épais. Ainsi que je pourrais augmenter les "dents" dans la partie inférieur.

Le design du modèle 4 me plaît beaucoup, par contre le dimensions sont encore trop petites. J'avais pensé pour améliorer, agrandir la taille de la poignée comme le premier modèle, ainsi qu'avoir la partie inférieure plus ovale au lieu d'avoir une base droit.

**2. Plateau:**

![](./images/FinalP.jpg)

La taille du plateau pourrait être plus allongée. Car quand je l'utilise il y a ma main qui est souvent en contacte avec les parois du plateau. Je pense que l'hauteur du plateau pourrait être la même que l'hauteur de la base du pylône pour réduire le contact entre la main et le plateau.

#### 3ÈME TEST

Après les remarques du pré-jury, j'ai décidé de continuer à travailler la forme et la taille des deux éléments Car l'ensemble ne marchait pas, la forme du pylône n'était pas adaptée à la forme et la taille de la main. Ainsi que le plateau était trop petit pour la création de sauces.

Pour le pylône mon idée initiale était d'agrandir la partie superieure et inférieure afin qu'il puisse écraser plus de nourriture et donc moins d'effort. J'avais aussi l'intention de changer le mouvement de la main afin que ce soit moins fatigant (voir schéma).

DESSIN

##### PROTOTYPES

  - **Pylon 01**

![](./images/F_Pylon1.jpg)


  J'ai bien aimé le design de l'objet mais il était un petit peu petit pour l'usage.    
  Normalement j'avais dessiné le modèle pour avoir la sphère dans la partie en contact avec la main (option 1). Mais finalement en parlant avec des amies j'ai remarqué qu'une courbature plus légère était plus adaptée à la forme de la main (option 2).

RESULTATS/TESTS

![](./images/Test01salsas.jpg)


  - Option 1:
   - Pas très pratique pour écraser les tomates ni les poivrons car la surface n'est pas si grande   
   - L'hauteur du pylône n'est pas très haute du cou c'est un peu dérangant:      
      - Facile le contact avec la nourriture      
      - La main est souvent en contact avec le plateau      
    - Pratique pour écraser les épices


  - Option 2:
    - La sphère glisse en contact avec le plateau, et ça prend plus de temps à écraser la nourriture.    
    - La forme inférieure est pratique pour écraser les épices mais pas pour les tomates ni les piments.   

Dans les deux options, la prise du pylône n'était pas très claire car quand je demandais des avis aux autres personnes il prenait le pylône d'autres manière. Aussi dans ce prototype c'est un peu compliqué de couper l'oignon.

  - **Plateau**

![](./images/F_FusionPlato1.jpg)

Pour ce test, j'ai décidé de faire le plateau plus grand pour que tous les ingrédients puissent entrer dans le bol (tomates, l'oignon et les piments). Pour la hauteur du plateau j'ai pris la même que largeur de ma main. Pour jouer un peu avec le design du projet, j'ai essayé aussi de désinner sur le modèle la prise de la main.

RESULTATS/TEST:

  - La prise de la main fonctionne pas, trop petite  
  - La taille est un peu grande pour les ingrédients  
  - Le pylône glisse en contact avec le plateau car ils n'ont pas la même forme du fond.

## IV. ADAPTATION

###### **PLATEAU**

![](./images/F_FusionPalto2.jpg)

Pour ce prototype, j'ai d'abord analysé comme on utilise l'objet. On a toujours la main droite qui prend le pylône et la main gauche qui prend le bowl afin que ceci ne bouge pas. Pour faire cet objet plus adapté sur le mouvement et la position de la main j'ai continué à travailler la prise en main.

Pour mieux visualiser la forme que le bowl peut avoir, j'ai fait un test en argile. Même si la forme n'est pas top, ce test m'aidait à mieux comprendre comment je pourrais faire le prototype en 3D.

![](./images/F_Test02Argile.jpg)

RESULTATS/TEST

![](./images/F_TestSalsasPlato2.jpg)

![](./images/F_PlateauTomates.jpg)

- Largueur ok (assez d'espace pour les aliments). L'hauteur du bol peut être augmenté parce que c'est le minimum.
- Forme de la prise en main ok
- Il peut être encore plus épais (4mm)
- Il manque un élément pour verser la sauce.

Un petit problème du bol est le fond, dans ce prototype j'ai fait la base plate et les pylônes avec une base arrondie. Ces deux manières ne marchent pas en ensemble. Du coup j'ai décidé de retravailler le pot avec la même forme extérieure mais plutôt une base courbée.
On peut voir dans les dimensions que j'ai agrandi surtout pour l'hauteur, pour que la main puisse vraiment prendre le contour de l'objet et éviter  le contact avec la nourriture. J'ai fait le fond plus épais pour que l'objet soit plus résitant.

* **Modification**

![](./images/F_FusionPlato3.jpg)

Pour ce design j'ai gardé quasi la même forme de l'extérieur et j'ai arrondi le fond intérieur. J'ai testé de jouer un peu avec la hauteur du bol (parti qui descent et qui monte) mais finalement j'ai décidé de ne pas continuer car ça rendait la création du moule plus coñpliqué.
J'ai aussi décidé de mettre un bec pour verser la sauce.

RESULTATS/TEST

![](./images/F_Plateau2Tomates.jpg)

J'ai bien aime ce plateau, la taille est ok, la prise en main est clair et le bec sert pour verser la sauce et poser le pylône.



###### **PYLON**

**TEST 01:**

![](./images/F_PTest01.jpg)

Je voulais tester autrefois ce prototype circulaire afin de changer le mouvement de la main. J'ai agrandi la taille du pylône et j'ai ajouté les marques du doigt.Pour ce modèle j'ai fait deux options, avec les dents et sans les dents.

RESULTATS/TESTS

![](./images/F_TestSalsas1.jpg)

**Option 1 (Avec les dents):**

  - POSITIVE:     
   - Il marche bien car le fond du plateau est la même que la forme du pylône      
   - Pratique pour les tomates et piments


  - NEGATIVE:      
    - Pas très beau niveau esthétique      
    - Un petit peu large pour le plateau     
   - Pas très haute donc plus facile d'avoir contact avec la nourriture      
   - Pas top pour les guacamoles et les épices


**Option 2 (Sans les dents)**:

  - POSITIVE:
      - Pratique pour faire de guacamoles      
      - Plus beau esthétiquement     
      - Avec les marques du droit on comprend mieux la prise  


  - NEGATIVE:     
   - Pour ce modèle du pylône le plateau ne marché pas très bien, il doit être pus allongé

**TEST 02**

![](./images/F_PTest02.jpg)

Pour ce modèle, j'ai agrandi au maximum l'hauteur afin que la main ne soit pas en contact avec la nourriture. La partie supérieur et inférieur ont quasi la même couvre et j'ai décidé d'ajouter la marque du droit afin que soient plus clairs la prise et le mouvement de la main.

RESULTATS/TESTS

![](./images/F_SalsaPylon2.jpg)

  - POSITIVE:    
    - Dimensions (hauteur et largeur) ok    
    - Forme de la partie inférieure et superior ok  
    - Il fonctionne très bien pour les épices, les tomates et les piments  
    - Pas beaucoup de temps pour écraser
    - Bonne forme supérieur pour la main (pas trop mal)


  - NEGATIVE:    
    - L'empreinte est trop marqué du coup ça fait  mal quand on utilise l'objet    
    - Il n'est pas très beau esthétiquement et je sens qu'il n'est pas en harmonie avec le dessin du plateau.

**TEST 03**

![](./images/F_Pest03.jpg)

- POSITIVE:  
    - Dimensions (hauteur) ok  
    - Forme de la partie inférieure et supérieure ok  
    - Il fonctionne très bien pour les épices, les tomates et les piments  
    - Je sens que ce modèle va mieux avec l'esthétique du plateau
    - Bonne forme supérieure pour la main (pas trop mal)
    - Pas beaucoup de temps pour écraser grâce à la taille


  - NEGATIVE:    
      - La marque du doigt est trop marqué du coup ça fait  mal quand on utilise l'objet    
      - Un petit peu grand niveau de la largeur 


* **Modification (Idée de cuillère)**

![](./images/F_PTest04C.jpg)

J'ai essayé de faire dans le modèle 3D une partie creusé pour faciliter le service de la sauce. Par contre dans le modèle imprimé on peut voir que c'est très petit et que ce n'est pas assez profond.

En faisant des sauces avec ce pylône, j'ai eu du mal. la cuillère n'était pas très utile.

![](./images/F_Salsas03.jpg)

**TEST 04**

Pour ce modèle j'ai pris un petit plus de temps pour dessiner l'objet pour avoir plus d'harmonie dans l'ensemble. Aussi j'ai essayé de faire plus d'attention aux remarques déjà mentionnées.

![](./images/F_FusionPylone4.PNG)

RESULTATS/TESTS

- POSITIVE:
  - Dimensions (hauteur) ok
  - Forme de la partie inférieure et supérieure ok
  - Il fonctionne très bien pour les épices, les tomates et les piments
  - Il me plait plus esthétiquement
  - L'empreinte est très soft


- NEGATIVE:
    - Il est  peut être petit pour des autres personnes mais pour moi la taille est bonne


* **Modification (Idée de cuillère)**

![](./images/F_Test04.jpg)

![](./images/F_Salsas04.jpg)

Comme dans l'autre prototype j'ai voulu ajouter la forme d'une cuillère  pour verser la sauce. Mon intention était d'avoir juste un objet qui pourrait être utile pour le service de la sauce, écraser les épices, tomates et piments.

Mais le prototype n'a pas bien marché, la taille de la "cuillère" est trop petite, elle est un petit peu haute et elle n'est pas assez profonde.

## V. REALISATION

Comme l'objet que j'ai choisi est en contact avec la nourriture, il faut que je choisisse bien la matière avec laquelle je vais réaliser l'objet.

  **1ers tests: Argile**

L'idée était d'imprimer l'objet en 3D et ensuite de l'utiliser comme moule pour le créer en argile.

Dessin

J'ai fait deux tests pour le plateau avec argile auto-durcissant. Le premier test donnait bien mais la matière n'était pas assez résistante pour l'usage.

![](./images/F_TestArgile01.jpg)

J'ai raté un peu le deuxième test car c'était compliqué de faire une forme à partir du plateau, pour faire ceci il fallait avoir l'argile liquide.

![](./images/F_Test02Argile.jpg)

J'ai contacté aussi à un atelier de céramique pour avoir un deuxième avis. Effectivement la matière n'était pas résistant pour l'usage, ainsi que la production en argile prend trop de temps de séchage ce qui n'était pas pratique pour moi.

Du coup, j'ai décidé de me renseigner un peu la matière que je pourrais utiliser. En faisant des recherches sur internet, je suis tombé sur une collection d'un céramiste belge d'ou je me suis inspiré.

![](./images/F_Reference.jpg)

Ensuite, j'ai regardé des résines ou huiles qui pourrait rendre la matière adapté au contact alimentaire.

#### LES MOULES

Ici je vais vous expliquer comment j'ai fait pour créer les moules en silicone, avec Fusion 360, les machines 3D et la machine thermoformeuse.

- **1er Test: Silicone**

L'idée de base était de créer un moule totalement en silicone pour ne pas avoir problème du décoffrage. Aussi je voulais faire comme ça pour ne pas avoir de problèmes avec la forme surtout pour le plateau (fond plat à l'extérieur et courbé à l'intérieur). J'ai choisi aussi de faire cette méthode car le temps d'impression d'un moule du plateau pourrait prend 1 jour et effectivement ce n'était pas l'idéal.

![](./images/F_Silicone.jpg)

Pour la méthode, j'ai fait un cube avec du carton très épais et j'ai mis le plateau à l'intérieur. Ensuite il faut bien mélanger la silicone avec son durcisseur et tout verser dans le cube.

    Quelques conseils:    
      - Utiliser des gants, car la matière est toxique et c'est très difficile de l'enlever avant le séchage.      
      - Il faut laisser sécher 24 h, mais ça dépend de chaque silicone    
      - Pour les quantités, tu peux utiliser du riz pour savoir combien de silicone il faut (moi je ne savais pas ça)

Malheureusement la technique n'a pas bien marché, en faisant le  mélange la silicone j'ai remarqué que même si j'avais acheté 1,5 Kg de silicone, le produit n'était pas assez pour le moule.

Du coup, j'ai décidé d'utiliser la silicone juste pour la partie intérieure et utiliser la machine thermoformeuse pour le moule extérieur.

![](./images/F_SiliconeTMold.jpg)

RESULTAT

![](./images/F_BetonT01.jpg)

Malheureusement ce test n'a pas très bien marché car le béton au moment du séchage poussait vers l'extérieur, il cassait le plastique et donc la forme s'est un peu déformée.

Même si la forme n'a pas très bien marché, niveau matière je suis très satisfait. Le béton est plus clair car j'ai mis de la poudre blanche (pigment blanc) pour changer la couleur.


- **2ème Test: Machines 3D**

Vu que le test avec la machine thermoformeuse n'a pas marché, j'ai continue à me renseigner de comment je pourrais faire les moules avec les machines 3D.

Je vais vous expliquer ici comment j'ai fait le moule extérieur sur **Fusion 360** car même si c'est super facile,  le processus m'a pris du temps.

Une fois que le design de l'objet est fini, tu cliques sur *écart* -> tu sélectionnes tous les surfaces utiles pour le moule -> *épaissir* (j'ai mis 1mm comment ça c'est plus vite à imprimer).

![](./images/F_Fusion.jpg)

Pour ne pas avoir des problèmes de décoffrage j'ai divisé en deux le plateau. Pour faire ceci avec fusion tu vas dans -> *Construire* -> *Plan moyen* -> tu choisis les deux faces extrêmes pour avoir le milieu du plateau -> *Diviser corps* (tu sélectionnes le plateau que tu viens de faire et le plan du milieu) -> et voilà normalement le plateau est divisé en deux

![](./images/F_FusionMoules.jpg)

Pour couler les deux parties ensemble j'ai décidé d'ajouter un petit bord afin que ce soit plus facile de coller les deux parties ensemble et que l'objet ne bouge pas.

![](./images/F_Mouleplato.jpg)

Pour les pylônes, j'ai fait plus au moins pareille. J'ai créé un cube autour du pylône -> *division de corps* (tu sélectionnes le cube et le pylône) -> Diviser le pylône en deux (tu sélectionnes le cube divisé et le plan du milieu -> tu peux changer les dimensions du cube avec l'outil de *extruder* pour avoir aussi une ouverture pour couler le béton.

Voici une photos des moules imprimés en 3D.

![](./images/Mouless.jpg)

RESULTATS

![](./images/F_BetonT02.jpg)

Je suis très satisfait du résultat, juste pour le pylône, le moule n'a pas fonctionné donc il y a une partie qui s'est cassé.

![](./images/F_Moule01.jpg)

Sur les photos, l'objet a été poncé et vernis.

- Processus:

![](./images/F_Betonprocesus.jpg)

1. Bien mettre le moule en silicone et le moule en plastique pour que celui ci ne bouge pas. Pour le moule en plastique j'ai utilisé de la tape pour que tout soit fixe et pour le moule en silicone je n'ai rien mis car il est lourd donc c'est compliqué qu'il bouge.
Si ça peut t'aider tu peux dessiner avec crayon où tu dois mettre le moule en plastique et le moule en silicone, comme ça la forme est plus respectée.

2. Mettre de l'huile sur tous les surfaces qui vont être en contact avec le béton.

3. Mélanger le béton avec un peu d'eau, pour ca je n'ai pas suivi des mesures exactes. Il faut juste que le béton ne soit pas trop liquide ni trop sec.
Pour cette étape j'ai ajouté du pigment blanc.  

4. Couler le béton, tu peux utiliser des piques en bois pour faire descendre le béton.

5. Super important de faire vibrer le béton, ça aide à faire descendre le béton, à enlever les bulles d'air et d'avoir une surface plus lisse.  

6. Attends entre 12h et 24h pour que le béton soit totalement sec. Pendant cette période tu peux laisser le béton à l'extérieur ou soit à l'intérieur mais pas près du chauffage. Dans mon cas, pour être sur j'ai attendu 24h et j'ai laissé le béton à l'intérieur avec un sac en plastique autour pour garder l'humidité.

7. Décoffrage doucement, avec les moules en plastique et silicone je n'ai eu aucun problème. Si les moules sont bien faits. Ensuite tu peux limer un peu la surface du béton et passer le vernis (j'ai passé le vernis au moins 3 fois sur tout pour les parties en contact avec la nourriture).


      Matériaux utilisés:
        - Béton pour des objets de la maison (qui peuvent être en contact avec la nourriture)
        - Pigments pour changer la couleur (J'ai acheté noir et blanc)
        - Vernis pour le béton qui peut être en contact alimentaire.
        - Huile de cuisine pour mettre dans les moules

![](./images/Maleriales.jpg)

## VI. OBJET FINALE

Grâce aux tests que j'ai réalisé et aux impresions 3D, j'ai fait quelques modifications sur le protoype finale.

Pour le plateau, j'ai décidé de garder l'intérieur concave afin de rendre plus éfficace le mélange des aliments. Pour l'extérieur j'ai gardé une courbe pour la paume de la main et aussi deux courbes pour la prise des doigts (un pour les gaucher et droitier).

Pour les pylônes, j'ai essayé d'avoir la même courbe dans la partie inférieur que dans le pot. Pour que le pylône puisse glisser plus facilement et prendre la nourriture qui reste dans le coin. J'ai aussi décidé d'agrandir un peu la partie inférieur et supérieur pour que pylône puisse écraser plus de nourriture avec moins d'effort.

Dans les deux pylônes ont peux voir l'empreinte du doigt pour que le mouvement de celui-ci soit plus clair. J'ai testé l'idée d'avoir une cuillère mais finalement je n'ai trouvé pas ca très utile et je sentais que ce n'était pas top dans le design de l'objet.

J'ai décidé aussi d'ajouter un couvercle pour éviter que les insectes puisse entrer dans le "molcajete". Mon primer idée était de le faire en bois car je sentais que les couleurs allait bien ensemble.

Pour faire ce prototype, j'ai fait le design sur **Fusion 360** -> exporter le document **SVG** et ensuite j'ai fait avec la machine **Shapper**. La taille du couvercle est un peu petit car la machine a coupé sur la ligné et pas dehors de la ligne.

![](./images/F_Machine.jpg)
![](./images/F_Bois.jpg)

Pour le couvercle, j'ai décide aussi de tester de le faire en béton et plutôt jouer avec les tonalités de la matière.

![](./images/Couvercle.jpg)

Les moules du couvercles, du bowl et des pylônes ont été fait par l'imprimante 3D et en suite j'ai juste coule le béton en ajoutant d'huile pour faciliter de décoffrage. Le premier test de couvercle n'a pas marché car je n'ai pas attendu les 24h et peut être aussi parce que l'objet il était pas assez épais.

Malheureusement, le prototype finale n'a pas marché correctement. Je pense que le mélange du béton n'a pas été bien réalisé car j'ai bien attendu les 24hrs avant le décoffrage :(((((((

![](./images/F_FinaleNo.jpg)

![](./images/F_Plateau.jpg)

Finalement, j'ai nommé mon objet **Molkiza**. Pour arrive à ce nom,  j'ai joué un peu avec le nom d'origine "molcajete" qui veut dire "pierre concave pour la sauce" et des autres mots de la culture mexicaine.


#### LIENS UTILES

**VIDEOS**

- [Béton avec des imprimantes 3D](https://www.youtube.com/watch?v=gVrJOnB1VHU)

- [Traviller l'argile à partir d'un moule](https://www.youtube.com/watch?v=FO_cNDrI-Ik)

- [Créer des moules avec des imprimantes 3D](https://www.youtube.com/watch?v=NmAiUgo0xUk&list=LL&index=6)

**FICHIERS**

- [Download Molcajete](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Documents/Molcajete.stl)

- [Download Pylon01](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Documents/Pylone01.stl)

- [Download Pylon02](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Documents/Pylon02.stl)


- [Download Base](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Documents/Base.stl)


- [Download Couvercle](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-design/students/maria.lopez.manzano/-/blob/main/docs/Documents/Couvercle.stl)


